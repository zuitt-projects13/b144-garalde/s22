=======USERS=======

{
	_id: objectID,
	isAdmin:Boolean,
	firstName: String,
	lastName:String,
	age:String,
	gender:String,
	email:String,
	password:String,
	mobileNo:String,
	deliveryAddress:[
			{
				unitNo:String,
				floorNo:String,
				houseBdlgNo:String,
				street:String,
				villageSubd:String,
				brgy:String,
				municipalityCity:String,
				province:String,
				landmark:String
			}
		],
	cart: [
			{
				productID:string,
				transactionID:string
			}
		]
}



======TRANSACTION======
{
	_id: objectID,
	userID:String,
	productID: String,
	isPaid: Boolean,
	totalAmount: Number,
	paymentMethod: String,
	dateTimeCreated: Date,
	dateTimePaid: Date,
	deliveryStatus:String,
	isCheckedOut:Boolean
}





========PRODUCT========

{
	_id: objectID,
	isActive: Boolean,
	productName: String,
	productCategory:String,
	productVolume:String,
	productDescription: String,
	productPrice: Number,
	productPhotoPath:
		{
			frontView:String,
			backView:String,
			leftView:String,
			rightView:String,
			topView:String,
			bottomView:String,
		}
}